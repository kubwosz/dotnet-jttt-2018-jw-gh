﻿using System;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Collections.Generic;

namespace JTTT
{
   
    public partial class Form1 : Form
    {
       

        public Form1()
        {
            InitializeComponent();

            fillListBoxTasksList();
        }

        BindingList<Task> listOfTasks = new BindingList<Task>();
        Log file1 = new Log();
        JtttDbContext ctx = new JtttDbContext();
        private void fillListBoxTasksList()
        {
         

                foreach (var t in ctx.Task)
                {
                    listOfTasks.Add(t);
                }

                listBoxTasksList.DataSource = listOfTasks;
            
        }

        private void buttonAddToList_Click(object sender, EventArgs e)
        {
            using (var ctx = new JtttDbContext())
            {

                if ((textBoxUrl.Text == "" || textBoxTextLookingFor.Text == "" || textBoxMail.Text == "" || textBoxTaskName.Text == "" || comboBoxActionType.SelectedItem == null || comboBoxConditionType.SelectedItem == null) && (textBoxMiasto.Text == "" || comboBoxTemperatura.SelectedItem == null || comboBoxConditionType.SelectedItem == null))
                {
                    MessageBox.Show("Proszę wypełnić wszystkie pola dla strony");
                }
                else if((comboBoxConditionType.SelectedIndex == 0 || comboBoxConditionType.SelectedIndex == 1) && (textBoxUrl.Text == "" || textBoxTextLookingFor.Text == "" || textBoxMail.Text == "" || textBoxTaskName.Text == "" || comboBoxActionType.SelectedItem == null))
                {
                    MessageBox.Show("Proszę wypełnić wszystkie pola dla strony");
                }
                else if((comboBoxConditionType.SelectedIndex == 2) &&(textBoxMiasto.Text == "" || comboBoxTemperatura.SelectedItem == null))
                {
                    MessageBox.Show("Proszę wypełnić wszystkie pola dla pogody");
                }
                else
                {

                    Mail mail = new Mail() { MailName = textBoxMail.Text };


                    ConditionType cnd = new ConditionType()
                    {
                        Index = comboBoxConditionType.SelectedIndex,
                        ConditionName = comboBoxConditionType.Text,
                        UrlPage = textBoxUrl.Text,
                        TextLookingFor = textBoxTextLookingFor.Text,
                        CityName = textBoxMiasto.Text,
                        TempValue = Int32.Parse(comboBoxTemperatura.Text)
                    };

                    ActionType act = new ActionType()
                    {
                        Index = comboBoxActionType.SelectedIndex,
                        ActionName = comboBoxActionType.Text,
                        Mail = mail
                    };

                    Picture pic = new Picture();
                    //WeatherCondition weather = new WeatherCondition() {CityName = textBoxMiasto.Text, TempValue = Int32.Parse(comboBoxTemperatura.Text), ConditionName = comboBoxConditionType.Text, Index = comboBoxConditionType.SelectedIndex };

                    if (comboBoxActionType.SelectedIndex == 1)
                    {
                        //StreamWriter sw = new StreamWriter(ActionType.GetSaveFileInformation().FileName);
                        //pic.FileName = ActionType.GetSaveFileInformation();
                        ActionType.GetSaveFileInformation(pic);
                    }


                    Task task = new Task()
                    {
                        // UrlPage = textBoxUrl.Text,
                        //  TextLookingFor = textBoxTextLookingFor.Text.ToLower(),
                        //Mail = mail,
                        Picture = pic,
                        NameOfTask = textBoxTaskName.Text,
                        Condition = cnd,
                        Action = act
                    };


                    if (checkBoxIsCaseSensitive.Checked)
                        task.Condition.TextLookingFor = textBoxTextLookingFor.Text;

                    file1.Write("Click Dodaj do listy");
                    file1.Write("Parametry zadania:");
                    if(comboBoxConditionType.SelectedIndex == 2 || comboBoxConditionType.SelectedIndex== -1)
                        file1.Write("Sprawdzenie pogody, TaskName: " + textBoxTaskName.Text + " Miasto: " + cnd.CityName + " Warunek temperatury: " + cnd.TempValue.ToString());
                    
                    else
                        file1.Write("Nazwa: " + textBoxTaskName.Text + ", Url: " + textBoxUrl.Text + ", Mail: " + textBoxMail.Text + ", Wyszukiwane słowo:" + textBoxTextLookingFor.Text);

                    listOfTasks.Add(task);
                    listBoxTasksList.DataSource = listOfTasks;

                    ctx.Task.Add(task);

                    ctx.SaveChanges();
                }
                
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ctx.Database.ExecuteSqlCommand("TRUNCATE TABLE [Tasks]");
            file1.Write("Click Clear");
            listOfTasks.Clear();
            listBoxTasksList.DataSource = listOfTasks;
        }

        private void buttonDo_Click(object sender, EventArgs e)
        {
            Uri u;

            file1.Write("Click Wykonaj!");

                foreach (var task in listOfTasks)
            {
                if (task.Condition.Index == 1 || task.Condition.Index == 0)
                {


                    file1.Write("log dla zadania:" + task);
                    string image = task.Picture.SearchForPicture(checkBoxIsCaseSensitive.Checked, task, file1);

                    if (image == "https://img1.demotywatoryfb.pl/default")
                    {
                        MessageBox.Show("Obraz nie został znaleziony", "Bład podczas szukania obrazu!");
                        file1.Write(" ER 001-Obraz nie został znaleziony, Błąd podczas szukanie obrazu!");
                    }
                    else
                    {
                        try
                        {
                            u = new Uri(image);
                            file1.Write("Proba pobrania: " + u.ToString());
                            if (Picture.DownloadRemoteImageFile(u.ToString(), "obrazekTmp.jpg") == false)
                            {
                                MessageBox.Show("Obraz nie został pobrany poprawnie", "Bład podczas pobierania obrazu!");
                                file1.Write("ER 002-Obraz nie został pobrany poprawnie, Bład podczas pobierania obrazu!");
                            }
                            else
                            {
                                file1.Write("Udało sie pobrac obrazek: " + u.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Obraz nie został pobrany poprawnie. Szczegółowe informacje:" + ex.Message, "Bład podczas pobierania obrazu!");
                            file1.Write("ER 003-Obraz nie został pobrany poprawnie, Bład podczas pobierania obrazu!");
                            //throw ex;
                            u = null;
                        }

                        task.DoAction(u);
                        file1.Write("------");
                    }
                }
                else
                {
                    string nazwaMiasta = task.Condition.CityName;
                    Pogoda p = new Pogoda();
                    if (p.DownloadWeather(nazwaMiasta))
                    {
                        FormActionDisplay form = new FormActionDisplay(p,(int)task.Condition.TempValue, task.Condition.CityName);
                        form.Show();
                    }
                    else
                    {
                        MessageBox.Show("Zła nazwa miasta");
                    }
                }
            }
        }

        public void Save<T>(T file, String path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (StreamWriter writer = new StreamWriter(path))
            {
                serializer.Serialize(writer, file);
            }
            MessageBox.Show("Serialize complite.");
        }

        public object Read(String path, Type type)
        {
            var serializer = new XmlSerializer(type);
            TextReader reader = new StreamReader(path);
            Object file;

            file = (Object)serializer.Deserialize(reader);
            reader.Close();

            return file;
        }

        private void buttonDeSerialize_Click(object sender, EventArgs e)
        {
            file1.Write("Click DeSerialize");
            listBoxTasksList.Items.Clear();

            BindingList<Task> listTemp = ((BindingList<Task>)Read("serializacja", listOfTasks.GetType()));
            MessageBox.Show("Deserialize complite.");
            file1.Write("Deserializacja: ");
            foreach (var l in listTemp) {
                listBoxTasksList.Items.Add(l);
                listOfTasks.Add(l);
                file1.Write(" Task_Name_Ad: " + l.NameOfTask);
            }
        }

        private void buttonSerialize_Click(object sender, EventArgs e)
        {
            file1.Write("Click Serialize");
            Save(listOfTasks, "serializacja");
        }

        private void buttonSampleTask_Click(object sender, EventArgs e)
        {
            Task task = new Task();
            //task.SampleTask();
            MessageBox.Show("Niezaimplementowane");
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            //FormActionDisplay form = new FormActionDisplay();
            //form.Show();

                FormDisplayForPage form = new FormDisplayForPage("ostatni pobrany", "demotywatory.pl");
                form.Show();
        
        }
    }
}
