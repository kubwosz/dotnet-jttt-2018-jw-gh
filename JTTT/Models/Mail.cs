﻿using System;
using System.Net;
using System.IO;
using System.Windows.Forms;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace JTTT
{
    public class Mail
    {
        public int Id { get; set; }
        public string MailName { get; set; }

        public bool SendMail(Log file2, string nameOfTask)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("Nadawca", "konto.net.laby@gmail.com"));
                message.To.Add(new MailboxAddress("Adresat", this.MailName));
                message.Subject = "W załączniku przesyłam zdjęcie - "+ nameOfTask;

                var builder = new BodyBuilder();

                builder.TextBody = @"Witam,

Wysyłam maila z żądanym załącznkiem z poziomu programu.

Z poważaniem,
JTTT 
                ";

                builder.Attachments.Add(@"obrazekTmp.jpg");
                message.Body = builder.ToMessageBody();


                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, j) => true;

                    client.Connect("smtp.gmail.com", 587, SecureSocketOptions.Auto);

                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    client.Authenticate("konto.net.laby", "dotnetlab");

                    client.Send(message);
                    client.Disconnect(true);

                    MessageBox.Show("Mail został wysłany poprawnie", "Potwiedzenie wysłania maila");
                    file2.Write("Mail został wysłany poprawnie");
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Mail NIE został wysłany poprawnie. Treść błędu:" + ex.Message, "Błąd przy wysyłaniu maila");
                file2.Write("Err 013 Task_SendMail Bad_work, message: " + ex.Message);
                //throw ex;
                return false;
            }
        }
    }
}
