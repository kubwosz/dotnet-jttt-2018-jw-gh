﻿using System;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel.DataAnnotations.Schema;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace JTTT
{
    public class Task
    {
        public int Id { get; set; }
        public string NameOfTask { get; set; }
        public int? PictureId { get; set; }
        [ForeignKey("PictureId")]
        public virtual Picture Picture { get; set; }
        public int? ConditionId { get; set; }
        [ForeignKey("ConditionId")]
        public virtual ConditionType Condition { get; set; }
        public int? ActionId { get; set; }
        [ForeignKey("ActionId")]
        public virtual ActionType Action { get; set; }

        public Log file2 = new Log();


        //public void SampleTask()
        //{
        //    using (var ctx = new JtttDbContext())
        //    {
        //        Task task;
        //        Mail mail = new Mail() { MailName = "nazwaDoMaila" };
        //        Picture picture = new Picture() { FileName = "nazwaPliku", PictureName = "nazwaObrazka" };
        //        ConditionType condition = new ConditionType() { ConditionName = "nawaWarunku", Index = 1 };
        //        ActionType action = new ActionType() { ActionName = "nazwaAkcji", Index = 1 };
        //        task = new Task()
        //        {
        //            UrlPage = "urlPage",
        //            TextLookingFor = "textlookingFor",
        //            NameOfTask = "nazwatasku",
        //            Mail = mail,
        //            Picture = picture,
        //            Condition = condition,
        //            Action = action
        //        };

        //        ctx.Task.Add(task);
        //        ctx.SaveChanges();
        //    }
        //}

        public void DoAction(Uri u)
        {
            if (Action.Index == 0)
            {
                file2.Write("Proba wysłania maila.");
                this.Action.Mail.SendMail(file2, NameOfTask);
            }
            else if (Action.Index == 1)
            {
                if (Picture.DownloadRemoteImageFile(u.ToString(), Picture.FileName) == false)
                    MessageBox.Show("Obraz nie został pobrany poprawnie", "Bład podczas pobierania obrazu!");
            }
            else if(Action.Index == 2)
            {
                FormDisplayForPage form = new FormDisplayForPage(Condition.TextLookingFor, Condition.UrlPage);
                form.Show();
            }
            else
            {
                MessageBox.Show("To nie powinno się pojawić.");
                file2.Write("Err 011 Task_DoAction Bad_work");
            }
        }


        public override string ToString()
        {
            string textTmp="";

            if (Condition.Index == 0)
            textTmp = Condition.UrlPage + "_" + Condition.TextLookingFor + "_" + this.Action.Mail.MailName;

            if (Condition.Index == 2)
                textTmp = textTmp + Condition.CityName + "_" + Condition.TempValue + "_";

            textTmp = textTmp + "_" + NameOfTask + "_" + Condition.ConditionName + "_" + Action.ActionName;

            return textTmp;
        }

    }
}
