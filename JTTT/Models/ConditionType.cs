﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace JTTT
{
    public class ConditionType
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string ConditionName { get; set; }
        public string UrlPage { get; set; }
        public string TextLookingFor { get; set; }
        public string CityName { get; set; }
        public int? TempValue { get; set; }

    }
}
