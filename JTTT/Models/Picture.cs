﻿using System;
using System.Net;
using System.IO;
using System.Windows.Forms;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace JTTT
{
    public class Picture
    {
        public int Id { get; set; }

        public string PictureName { get; set; }
        public string FileName { get; set; }


        public string SearchForPicture(bool isCaseSensitive, Task task, Log file2)
        {
            try
            {
                var doc = new HtmlAgilityPack.HtmlDocument();
                var html = new WebClient();

                var pageHtml = html.DownloadString(task.Condition.UrlPage);

                if (isCaseSensitive)
                    doc.LoadHtml(pageHtml);
                else
                    doc.LoadHtml(pageHtml.ToLower());


                var nodes = doc.DocumentNode.Descendants("img");
                string image = "default";

                if (task.Condition.Index == 0)
                {
                    foreach (var node in nodes)
                    {
                        var text = node.GetAttributeValue("alt", "");
                        if (node.GetAttributeValue("alt", "").Contains(task.Condition.TextLookingFor))
                        {
                            image = node.GetAttributeValue("src", "");
                            break;
                        }
                    }

                    if (!image.Contains("demotywatory"))
                        image = "https://img1.demotywatoryfb.pl/" + image;

                    return image;
                }
                else if (task.Condition.Index == 1)
                {
                    foreach (var node in nodes)
                    {
                        if (node.GetAttributeValue("href", "").Contains(task.Condition.TextLookingFor))
                        {
                            task.Picture.PictureName = node.GetAttributeValue("src", "");
                            break;
                        }
                    }
                    return image;
                }
                else
                {
                    MessageBox.Show("To nie powinno się pojawić.");
                    file2.Write("Err 012 Task_SheareForPicture Bad_work");
                    return "error";
                }

            }
            catch(Exception exc)
            {
                file2.Write("Err 013 Task_SheareForPicture Bad_work");
                MessageBox.Show("Błąd: " + exc.Message, "Błąd");
                return "error";
            }
        }


        public static bool DownloadRemoteImageFile(string uri, string fileName)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception)
            {
                return false;
            }

            if ((response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Moved ||
                response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {

                // if the remote file was found, download it
                using (Stream inputStream = response.GetResponseStream())
                using (Stream outputStream = File.OpenWrite(fileName))
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    do
                    {
                        bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                        outputStream.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
