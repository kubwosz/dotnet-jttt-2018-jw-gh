﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace JTTT
{
    public class ActionType
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string ActionName { get; set; }
        public int? MailId { get; set; }
        [ForeignKey("MailId")]
        public virtual Mail Mail { get; set; }


        static public SaveFileDialog GetSaveFileInformation(Picture pic)
        {
            SaveFileDialog savefile = new SaveFileDialog();

            savefile.FileName = "nazwa_pliku";
            savefile.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp";

            savefile.ShowDialog();

            pic.FileName = savefile.FileName;

            return savefile;

        }
    }
}
