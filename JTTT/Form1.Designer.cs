﻿namespace JTTT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.textBoxTextLookingFor = new System.Windows.Forms.TextBox();
            this.checkBoxIsCaseSensitive = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAddToList = new System.Windows.Forms.Button();
            this.buttonDo = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonDeSerialize = new System.Windows.Forms.Button();
            this.buttonSerialize = new System.Windows.Forms.Button();
            this.textBoxTaskName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.listBoxTasksList = new System.Windows.Forms.ListBox();
            this.buttonSampleTask = new System.Windows.Forms.Button();
            this.tabStrona = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.comboBoxTemperatura = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxMiasto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonShow = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxMail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxConditionType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxActionType = new System.Windows.Forms.ComboBox();
            this.tabControlBox = new System.Windows.Forms.TabControl();
            this.tabStrona.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControlBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxUrl
            // 
            this.textBoxUrl.Location = new System.Drawing.Point(81, 90);
            this.textBoxUrl.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.Size = new System.Drawing.Size(225, 22);
            this.textBoxUrl.TabIndex = 0;
            // 
            // textBoxTextLookingFor
            // 
            this.textBoxTextLookingFor.Location = new System.Drawing.Point(81, 138);
            this.textBoxTextLookingFor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTextLookingFor.Name = "textBoxTextLookingFor";
            this.textBoxTextLookingFor.Size = new System.Drawing.Size(225, 22);
            this.textBoxTextLookingFor.TabIndex = 1;
            // 
            // checkBoxIsCaseSensitive
            // 
            this.checkBoxIsCaseSensitive.AutoSize = true;
            this.checkBoxIsCaseSensitive.Location = new System.Drawing.Point(325, 130);
            this.checkBoxIsCaseSensitive.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxIsCaseSensitive.Name = "checkBoxIsCaseSensitive";
            this.checkBoxIsCaseSensitive.Size = new System.Drawing.Size(133, 38);
            this.checkBoxIsCaseSensitive.TabIndex = 5;
            this.checkBoxIsCaseSensitive.Text = "Zwracaj uwagę\r\n na wielkość liter";
            this.checkBoxIsCaseSensitive.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tekst:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "URL:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(447, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Na danej stornie znajduje sie obrazek , ktorego podpis zawiera tresc :";
            // 
            // buttonAddToList
            // 
            this.buttonAddToList.Location = new System.Drawing.Point(348, 564);
            this.buttonAddToList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAddToList.Name = "buttonAddToList";
            this.buttonAddToList.Size = new System.Drawing.Size(156, 55);
            this.buttonAddToList.TabIndex = 13;
            this.buttonAddToList.Text = "Dodaj do listy";
            this.buttonAddToList.UseVisualStyleBackColor = true;
            this.buttonAddToList.Click += new System.EventHandler(this.buttonAddToList_Click);
            // 
            // buttonDo
            // 
            this.buttonDo.Location = new System.Drawing.Point(844, 279);
            this.buttonDo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDo.Name = "buttonDo";
            this.buttonDo.Size = new System.Drawing.Size(107, 52);
            this.buttonDo.TabIndex = 15;
            this.buttonDo.Text = "Wykonaj!";
            this.buttonDo.UseVisualStyleBackColor = true;
            this.buttonDo.Click += new System.EventHandler(this.buttonDo_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(956, 281);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(116, 52);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "Czyść";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonDeSerialize
            // 
            this.buttonDeSerialize.Location = new System.Drawing.Point(1100, 281);
            this.buttonDeSerialize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDeSerialize.Name = "buttonDeSerialize";
            this.buttonDeSerialize.Size = new System.Drawing.Size(104, 23);
            this.buttonDeSerialize.TabIndex = 17;
            this.buttonDeSerialize.Text = "DeSerialize";
            this.buttonDeSerialize.UseVisualStyleBackColor = true;
            this.buttonDeSerialize.Click += new System.EventHandler(this.buttonDeSerialize_Click);
            // 
            // buttonSerialize
            // 
            this.buttonSerialize.Location = new System.Drawing.Point(1100, 309);
            this.buttonSerialize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSerialize.Name = "buttonSerialize";
            this.buttonSerialize.Size = new System.Drawing.Size(104, 23);
            this.buttonSerialize.TabIndex = 18;
            this.buttonSerialize.Text = "Serialize";
            this.buttonSerialize.UseVisualStyleBackColor = true;
            this.buttonSerialize.Click += new System.EventHandler(this.buttonSerialize_Click);
            // 
            // textBoxTaskName
            // 
            this.textBoxTaskName.Location = new System.Drawing.Point(43, 597);
            this.textBoxTaskName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTaskName.Name = "textBoxTaskName";
            this.textBoxTaskName.Size = new System.Drawing.Size(203, 22);
            this.textBoxTaskName.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 564);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Wybierz nazwę zadania:";
            // 
            // listBoxTasksList
            // 
            this.listBoxTasksList.FormattingEnabled = true;
            this.listBoxTasksList.ItemHeight = 16;
            this.listBoxTasksList.Location = new System.Drawing.Point(516, 30);
            this.listBoxTasksList.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxTasksList.Name = "listBoxTasksList";
            this.listBoxTasksList.Size = new System.Drawing.Size(715, 228);
            this.listBoxTasksList.TabIndex = 20;
            // 
            // buttonSampleTask
            // 
            this.buttonSampleTask.Location = new System.Drawing.Point(1100, 625);
            this.buttonSampleTask.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSampleTask.Name = "buttonSampleTask";
            this.buttonSampleTask.Size = new System.Drawing.Size(107, 52);
            this.buttonSampleTask.TabIndex = 21;
            this.buttonSampleTask.Text = "Przykladowy task do bazy";
            this.buttonSampleTask.UseVisualStyleBackColor = true;
            this.buttonSampleTask.Click += new System.EventHandler(this.buttonSampleTask_Click);
            // 
            // tabStrona
            // 
            this.tabStrona.AccessibleDescription = "";
            this.tabStrona.AccessibleName = "";
            this.tabStrona.Controls.Add(this.tabPage1);
            this.tabStrona.Controls.Add(this.tabPage2);
            this.tabStrona.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabStrona.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabStrona.Location = new System.Drawing.Point(32, 75);
            this.tabStrona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabStrona.Name = "tabStrona";
            this.tabStrona.SelectedIndex = 0;
            this.tabStrona.Size = new System.Drawing.Size(484, 229);
            this.tabStrona.TabIndex = 22;
            this.tabStrona.Tag = "";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.checkBoxIsCaseSensitive);
            this.tabPage1.Controls.Add(this.textBoxUrl);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.textBoxTextLookingFor);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(476, 200);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Strona";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.comboBoxTemperatura);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.textBoxMiasto);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(476, 200);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pogoda";
            // 
            // comboBoxTemperatura
            // 
            this.comboBoxTemperatura.FormattingEnabled = true;
            this.comboBoxTemperatura.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.comboBoxTemperatura.Location = new System.Drawing.Point(139, 130);
            this.comboBoxTemperatura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxTemperatura.Name = "comboBoxTemperatura";
            this.comboBoxTemperatura.Size = new System.Drawing.Size(72, 24);
            this.comboBoxTemperatura.TabIndex = 7;
            this.comboBoxTemperatura.Text = "0";
            this.comboBoxTemperatura.ValueMember = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 17);
            this.label11.TabIndex = 6;
            this.label11.Text = "Temperatura:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "Miasto:";
            // 
            // textBoxMiasto
            // 
            this.textBoxMiasto.Location = new System.Drawing.Point(139, 94);
            this.textBoxMiasto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxMiasto.Name = "textBoxMiasto";
            this.textBoxMiasto.Size = new System.Drawing.Size(197, 22);
            this.textBoxMiasto.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 16);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(333, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Temperatura w tym mieście jest wyższa niż wartość:";
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(176, 104);
            this.buttonShow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(107, 52);
            this.buttonShow.TabIndex = 24;
            this.buttonShow.Text = "Pokaż komunikat";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.LightGray;
            this.tabPage4.Controls.Add(this.buttonShow);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Size = new System.Drawing.Size(472, 190);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Pokaż komunikat";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(11, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(444, 39);
            this.label12.TabIndex = 0;
            this.label12.Text = "Pokaż komunikat na ekranie";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightGray;
            this.tabPage3.Controls.Add(this.textBoxMail);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.comboBoxActionType);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Size = new System.Drawing.Size(472, 190);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Wysli mail";
            // 
            // textBoxMail
            // 
            this.textBoxMail.Location = new System.Drawing.Point(192, 130);
            this.textBoxMail.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMail.Name = "textBoxMail";
            this.textBoxMail.Size = new System.Drawing.Size(253, 22);
            this.textBoxMail.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 133);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "e-mail:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 33);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Wybierz typ waunku:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 87);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(440, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Wyslij na podany adres e-mail wiadomosc ze znalezionym obrazkiem";
            // 
            // comboBoxConditionType
            // 
            this.comboBoxConditionType.FormattingEnabled = true;
            this.comboBoxConditionType.Items.AddRange(new object[] {
            "Sprawdź opis obrazka na stronie",
            "Sprawdź nazwę obrazka na stronie",
            "Sprawdź pogodę"});
            this.comboBoxConditionType.Location = new System.Drawing.Point(242, 30);
            this.comboBoxConditionType.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxConditionType.Name = "comboBoxConditionType";
            this.comboBoxConditionType.Size = new System.Drawing.Size(253, 24);
            this.comboBoxConditionType.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 41);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Wybierz typ akcji:";
            // 
            // comboBoxActionType
            // 
            this.comboBoxActionType.FormattingEnabled = true;
            this.comboBoxActionType.Items.AddRange(new object[] {
            "Wyślij wiadomość email z załącznikiem",
            "Zapisz plik na dysku w podanej lokalizacji",
            "Wyswietl komunikat z obrazkiem"});
            this.comboBoxActionType.Location = new System.Drawing.Point(192, 38);
            this.comboBoxActionType.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxActionType.Name = "comboBoxActionType";
            this.comboBoxActionType.Size = new System.Drawing.Size(253, 24);
            this.comboBoxActionType.TabIndex = 11;
            // 
            // tabControlBox
            // 
            this.tabControlBox.Controls.Add(this.tabPage3);
            this.tabControlBox.Controls.Add(this.tabPage4);
            this.tabControlBox.Location = new System.Drawing.Point(32, 341);
            this.tabControlBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControlBox.Name = "tabControlBox";
            this.tabControlBox.SelectedIndex = 0;
            this.tabControlBox.Size = new System.Drawing.Size(480, 219);
            this.tabControlBox.TabIndex = 23;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1248, 698);
            this.Controls.Add(this.tabControlBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tabStrona);
            this.Controls.Add(this.buttonSampleTask);
            this.Controls.Add(this.comboBoxConditionType);
            this.Controls.Add(this.listBoxTasksList);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxTaskName);
            this.Controls.Add(this.buttonSerialize);
            this.Controls.Add(this.buttonDeSerialize);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonDo);
            this.Controls.Add(this.buttonAddToList);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabStrona.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabControlBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.TextBox textBoxTextLookingFor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAddToList;
        private System.Windows.Forms.Button buttonDo;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonDeSerialize;
        private System.Windows.Forms.Button buttonSerialize;
        private System.Windows.Forms.TextBox textBoxTaskName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox listBoxTasksList;
        private System.Windows.Forms.CheckBox checkBoxIsCaseSensitive;
        private System.Windows.Forms.Button buttonSampleTask;
        private System.Windows.Forms.TabControl tabStrona;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxMiasto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxTemperatura;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox textBoxMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxConditionType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxActionType;
        private System.Windows.Forms.TabControl tabControlBox;
    }
}

