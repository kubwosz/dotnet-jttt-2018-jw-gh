﻿namespace JTTT
{
    partial class FormDisplayForPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxForPage = new System.Windows.Forms.RichTextBox();
            this.pictureBoxForPage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxForPage)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxForPage
            // 
            this.richTextBoxForPage.Location = new System.Drawing.Point(47, 62);
            this.richTextBoxForPage.Name = "richTextBoxForPage";
            this.richTextBoxForPage.Size = new System.Drawing.Size(856, 55);
            this.richTextBoxForPage.TabIndex = 0;
            this.richTextBoxForPage.Text = "";
            // 
            // pictureBoxForPage
            // 
            this.pictureBoxForPage.Location = new System.Drawing.Point(47, 123);
            this.pictureBoxForPage.Name = "pictureBoxForPage";
            this.pictureBoxForPage.Size = new System.Drawing.Size(856, 810);
            this.pictureBoxForPage.TabIndex = 1;
            this.pictureBoxForPage.TabStop = false;
            // 
            // FormDisplayForPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 996);
            this.Controls.Add(this.pictureBoxForPage);
            this.Controls.Add(this.richTextBoxForPage);
            this.Name = "FormDisplayForPage";
            this.Text = "FormDisplayForPage";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxForPage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxForPage;
        private System.Windows.Forms.PictureBox pictureBoxForPage;
    }
}