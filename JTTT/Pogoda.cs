﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JTTT
{
    public class Pogoda
    {

        public double tempC;
        public double tempK;
        public string icon;
        public string opis;
        public int wilgotnosc;
        public int cisnienie;
        public double wiatr;

        private string apikey = "3a8294d91998e4dbbe14535b6d2eb413";

        private Log file1 = new Log();
        public Pogoda()
        {
            tempC = 0;
            tempK = 0;
            icon = "";
            opis = "";
            wilgotnosc = 0;
            cisnienie = 0;
            wiatr = 0;
        }

        private void Convert(string json)
        {
             JsonClass deserializedProduct = JsonConvert.DeserializeObject<JsonClass>(json);
            JObject rss = JObject.Parse(json);
            tempK = (double)rss["main"]["temp"];
            tempC = tempK - 273.15;
            List<Weather> op = new List<Weather>();
            op = deserializedProduct.weather;
            wilgotnosc = (int)rss["main"]["humidity"];
            icon = op[0].icon;
            opis = op[0].description;
            cisnienie = (int)rss["main"]["pressure"];
            wiatr = (double)rss["wind"]["speed"];
        }

        public bool DownloadWeather(string CityName)
        {
            bool work = false;
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + CityName + ",pl&APPID=" + apikey;
            var html = new WebClient();
            try
            {
                var pageHtml = html.DownloadString(url);
                this.Convert(pageHtml);
                work = true;
            }
            catch
            {
                file1.Write("Err_Pogoda_001 - zły adres strony (Nazwa miasta moze byc zła)");
                work = false;
            }
            return work;
        }

    }
}
