﻿using System.Data.Entity;

namespace JTTT
{
    public class JtttDbContext : DbContext
    {
        public JtttDbContext()
            : base("jtttDB3000")
        {
           // Database.SetInitializer(new JtttDbInitializer());
        }

        public DbSet<Task> Task { get; set; }
        public DbSet<Mail> Mail { get; set; }
        public DbSet<Picture> Picture { get; set; }
        public DbSet<ActionType> ActionType { get; set; }
        public DbSet<ConditionType> ConditionType { get; set; }

    }
}
