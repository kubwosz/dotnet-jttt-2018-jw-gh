﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JTTT
{
    public partial class FormActionDisplay : Form
    {
           
        public FormActionDisplay(Pogoda p , int warunek, string CityN)
        {
            InitializeComponent();
            string text = "Temperatura w "+ CityN +": " + ChooseText(p) + ", akutalnie:" + p.tempC;
            if (p.tempC < warunek)
                text += "  -Warunek nie spełniony-";
            else
                text += "  -Warunek spełniony-";
            richTextBoxDescription.Text = text;
            pictureBoxAttachment.Load("http://openweathermap.org/img/w/" + p.icon + ".png");
        }

        string ChooseText(Pogoda p)
        {
            string s="";
            if (p.tempC <= 5)
                s = "bardzo niska";
            else if (p.tempC > 5 && p.tempC <= 17)
                s = "umiarkowana";
            else if (p.tempC > 17)
                s = "wysoka";

            return s;
        }
    }
}
