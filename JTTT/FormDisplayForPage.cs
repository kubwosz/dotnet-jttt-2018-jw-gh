﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JTTT
{
    public partial class FormDisplayForPage : Form
    {
        public FormDisplayForPage(string LookingFor,string Url)
        {
            InitializeComponent();
            richTextBoxForPage.Text = "Obrazek dla: " + LookingFor + " ze strony: " + Url;
            pictureBoxForPage.LoadAsync("./obrazekTmp.jpg");
        }
    }
}
